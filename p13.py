from intcode_vm import VM, YieldCode
from collections import defaultdict, namedtuple

Point = namedtuple('Point', 'x y')

def get_program():
    return list(map(int, open("data13.txt").read().strip().split(',')))

def print_board(board):
    x_max = int(max(board.keys(), key=lambda p: p.x).x)
    y_max = int(max(board.keys(), key=lambda p: p.y).y)

    for y in range(y_max+1):
        l = []
        for x in range(x_max+1):
            pixel = board[Point(x,y)]
            l.append(pixel)

        print(''.join(map(str, l)).replace('0', ' ').replace('1', '|').replace('2', '#').replace('3', '_').replace('4', 'O'))
    print("Score", board[Point(-1,0)])

def run_game(program):

    vm = VM(program)
    p = vm.run_intcode()
    board = defaultdict(int)

    for x in p:
        if x == YieldCode.Input:
            panel_x = list(board.keys())[list(board.values()).index(3)].x
            ball_x = list(board.keys())[list(board.values()).index(4)].x
            move_x = 0 if panel_x == ball_x else 1 if ball_x > panel_x else -1
            x = p.send(move_x)

        if x == YieldCode.Halting:
            break
        else:
            y = next(p)
            t = next(p)
            board[Point(x, y)] = t

    return board

program = get_program()
print(list(run_game(program).values()).count(2))

program = get_program()
program[0] = 2
print(run_game(program)[Point(-1,0)])
