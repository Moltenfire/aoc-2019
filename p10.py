from collections import namedtuple
from math import gcd, degrees, atan2

Point = namedtuple('Point', 'x y')
Vector = namedtuple('Vector', 'u v')

def get_asteroids():
    with open("data10.txt") as f:
        data = list(map(lambda l : l.strip(), f.readlines()))

    asteroids = []
    for y, line in enumerate(data):
        for x, a in enumerate(line):
            if a != '.':
                asteroids.append(Point(x,y))

    return asteroids

def get_angle(d):
    return (degrees(atan2(d.v, d.u)) - 90) % 360

def get_unit_direction(a):
    g = gcd(a.u, a.v)
    return Vector(a.u // g, a.v // g)

def get_direction(a, b):
    return Vector(a.x - b.x, a.y - b.y)

def find_visable(asteroids, a0):

    directions = []
    for a1 in asteroids:
        if a0 != a1:
            d = get_direction(a0, a1)
            dist = d.u + d.v
            directions.append((dist, get_unit_direction(d), a1))

    directions.sort()

    visable_dir = set()
    visable = {}
    for d in directions:
        if d[1] not in visable_dir:
            visable_dir.add(d[1])
            visable[d[2]] = d[1]

    return len(visable), visable

asteroids = get_asteroids()
results = { a : find_visable(asteroids, a) for a in asteroids }

best_position = max(results.items(), key=lambda x : x[1][0])
total_visable, visable_directions = best_position[1]
print(total_visable)

angles = sorted([(get_angle(d), a, d) for a, d in visable_directions.items()])
a = angles[199][1]

print(a.x * 100 + a.y)
