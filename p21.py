from asciicode_vm import AsciiVM

def get_program():
    return list(map(int, open("data21.txt").read().strip().split(',')))

def run_commands(commands):
    program = get_program()
    vm = AsciiVM(program)

    for command in commands:
        vm.run(command)

    return vm.output[-1]

# (!A | !B | !C) & D
commands_a = [
"NOT A J",
"NOT B T",
"OR T J",
"NOT C T",
"OR T J",
"AND D J",
"WALK"
]

# ((!A | !B | !C) & D) & (E | H)
commands_b = [
"NOT A J",
"NOT B T",
"OR T J",
"NOT C T",
"OR T J",
"AND D J",
"AND E T",
"OR H T",
"AND T J",
"RUN"
]

print(run_commands(commands_a))
print(run_commands(commands_b))
