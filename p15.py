from intcode_vm import VM, YieldCode
import networkx as nx
from collections import namedtuple, defaultdict, deque

Point = namedtuple('Point', 'x y')

opposite_direction = {
    1 : 2,
    2 : 1,
    3 : 4,
    4 : 3,
}

def get_program():
    return list(map(int, open("data15.txt").read().strip().split(',')))

program = get_program()
vm = VM(program)
p = vm.run_intcode()
next(p)

def move_direction(i):
    res = p.send(i)
    next(p)
    return res

def get_surrounding_points(p):
    return {
        Point(p.x, p.y-1) : 1,
        Point(p.x, p.y+1) : 2,
        Point(p.x-1, p.y) : 3,
        Point(p.x+1, p.y) : 4,
    }

def print_board(board, current_point):
    x_min = int(min(board.keys(), key=lambda x: x.x).x)
    x_max = int(max(board.keys(), key=lambda x: x.x).x)
    y_min = int(min(board.keys(), key=lambda x: x.y).y)
    y_max = int(max(board.keys(), key=lambda x: x.y).y)

    print()
    for y in range(y_min, y_max+1):
        l = []
        for x in range(x_min, x_max+1):
            p = Point(x,y)
            pixel = 3 if p == current_point else board[p]
            # pixel = board[p]
            l.append(pixel)

        print(''.join(map(str, l)).replace('0', '#').replace('1', '.').replace('2', '2'))
    print()

start_point = Point(0, 0)
current_point = start_point
board = defaultdict(int)
way_back = deque()

system_point = None

G = nx.Graph()

while True:
    surrounding_points = get_surrounding_points(current_point)
    unknown_surrounding_points = set(k for k, v in surrounding_points.items() if k not in board)

    if unknown_surrounding_points:
        next_point = unknown_surrounding_points.pop()

        direction = surrounding_points[next_point]
        res = move_direction(direction)
        board[next_point] = res

        if res > 0:
            G.add_edge(current_point, next_point)
            way_back.append((current_point, opposite_direction[direction]))
            current_point = next_point

            if res == 2:
                system_point = current_point

    elif not way_back:
        break
    else:
        next_point, direction = way_back.pop()
        move_direction(direction)
        current_point = next_point

l = nx.shortest_path_length(G, start_point, system_point)
print(l)
print(max(nx.single_source_shortest_path_length(G, system_point).values()))
