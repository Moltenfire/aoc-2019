from collections import defaultdict, namedtuple, deque
import networkx as nx
from copy import deepcopy
from pprint import pprint
from itertools import combinations

Point = namedtuple('P', 'x y')
Dist = namedtuple('Dist', 'k, d')

class Path():
    def __init__(self, current, path, length):
        self.current = current
        self.path = path
        self.length = length

    def get_state(self):
        unique_state = (self.current, self.path)
        return unique_state

    def path_length(self):
        return bin(self.path).count("1")

    def collected_keys(self):
        return self.path

    def __repr__(self):
        return str(self.current) + " " + str(bin(self.path)) + " : " + str(self.length)

def get_grid():
    grid = defaultdict(int)
    keys = {}
    doors = {}
    start_point = None
    with open("data18a.txt") as f:
        for y, line in enumerate(f.readlines()):
            for x, c in enumerate(line.strip()):
                if c != '#':
                    p = Point(x,y)
                    grid[p] = 1
                    if c == '@':
                        start_point = p
                    elif c != '.':
                        o = ord(c)
                        if o >= 97:
                            keys[o - 97] = p
                        else:
                            doors[o - 65] = p

    return grid, keys, doors, start_point, x, y

def get_surrounding_points(p):
    return set([
        Point(p.x, p.y-1),
        Point(p.x, p.y+1),
        Point(p.x-1, p.y),
        Point(p.x+1, p.y),
    ])

def build_graph(grid, max_x, max_y):
    edges = []
    for x in range(max_x+1):
        for y in range(max_y+1):
            p = Point(x,y)
            if grid[p]:
                for sp in get_surrounding_points(p):
                    if grid[sp]:
                        edges.append((p,sp))
    return nx.Graph(edges)

def get_distance(G, p0, p1, doors):
    path = nx.shortest_path(G, p0, p1)
    path_set = set(path)
    doors_in_way = 0
    for k, p in doors.items():
        if p in path_set:
            doors_in_way += (1 << k)
    distance = len(path) - 1
    return distance, doors_in_way

def get_key_to_key(G, keys, doors, start_point, start_point_num):
    key_to_key = defaultdict(dict)

    for k, p in keys.items():
        distance, doors_in_way = get_distance(G, start_point, p, doors)
        key_to_key[start_point_num][k] = (distance, doors_in_way)

    for k0, k1 in combinations(keys.keys(), 2):
        distance, doors_in_way = get_distance(G, keys[k0], keys[k1], doors)
        key_to_key[k0][k1] = (distance, doors_in_way)
        key_to_key[k1][k0] = (distance, doors_in_way)

    return key_to_key

def find_next_possible_moves(key_to_key, path):
    collected_keys = path.collected_keys()
    for k, v in key_to_key[path.current].items():
        if not (1 << k) & collected_keys:
            dist, doors_in_way = v

            if doors_in_way & collected_keys == doors_in_way:
                yield Dist(k, path.length + dist)

grid, keys, doors, start_point, max_x, max_y = get_grid()
G = build_graph(grid, max_x, max_y)

total_keys = len(keys)
start_point_num = -1
current_point = start_point

key_to_key = get_key_to_key(G, keys, doors, start_point, start_point_num)

full_paths = []
start_path = Path(start_point_num, 0, 0)
possible_paths = deque([start_path])

min_full_path_length = 1000000000000
min_path_lengths = defaultdict(int)

counter = 0
while possible_paths:
    counter += 1

    path = possible_paths.pop()

    possible_moves = find_next_possible_moves(key_to_key, path)
    possible_moves = (m for m in possible_moves if m.d < min_full_path_length)

    for m in possible_moves:
        new_path = Path(m.k, path.path + (1 << m.k), m.d)
        unique_state = new_path.get_state()

        better_path = False
        if unique_state in min_path_lengths:
            if new_path.length < min_path_lengths[unique_state]:
                min_path_lengths[unique_state] = new_path.length
                better_path = True
        else:
            min_path_lengths[unique_state] = new_path.length
            better_path = True

        if better_path:
            if new_path.path_length() == total_keys:
                min_full_path_length = new_path.length
                full_paths.append(new_path)
            else:
                possible_paths.append(new_path)

print("Iterations:", counter)
print("Min path length:", min([p.length for p in full_paths]))
