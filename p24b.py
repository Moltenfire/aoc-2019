from collections import defaultdict, namedtuple
from pprint import pprint
import networkx as nx

Point = namedtuple('P', 'x y z')

def get_grid():
    with open("data24.txt") as f:
        grid = defaultdict(int)
        for y, line in enumerate(f.readlines()):
            line = line.strip()
            for x, c in enumerate(line):
                grid[Point(x,y,0)] = 1 if c == "#" else 0
    return grid

def print_grid(grid, layer):
    for y in range(5):
        for x in range(5):
            if x == 2 and y == 2:
                c = '?'
            else:
                c = '#' if grid[Point(x,y,layer)] else '.'
            print(c, end='')
        print()

def get_surrounding_points(p):
    return set([
        Point(p.x, p.y-1, p.z),
        Point(p.x, p.y+1, p.z),
        Point(p.x-1, p.y, p.z),
        Point(p.x+1, p.y, p.z),
    ])

def get_layers(_min, _max):
    layer_nodes = set()
    for i in range(_min, _max + 1):
        for y in range(5):
            for x in range(5):
                if x == 2 and y == 2:
                    continue
                layer_nodes.add(Point(x,y,i))

    edges = set()
    for p in layer_nodes:
        for sp in get_surrounding_points(p):
            if sp in layer_nodes:
                edges.add((p, sp))

    for i in range(_min + 1, _max + 1):
        px_north = Point(2,1,i)
        px_south = Point(2,3,i)
        for x in range(5):
            pn = Point(x, 0, i - 1)
            edges.add((px_north, pn))
            ps = Point(x, 4, i - 1)
            edges.add((px_south, ps))

        py_west = Point(1,2,i)
        py_east = Point(3,2,i)
        for y in range(5):
            pw = Point(0, y, i - 1)
            edges.add((py_west, pw))
            pe = Point(4, y, i - 1)
            edges.add((py_east, pe))

    G = nx.Graph(list(edges))
    # print(nx.info(G))
    return G

def update_grid(grid, G):
    new_grid = defaultdict(int)

    for p in G.nodes():
        bug = grid[p]
        surrounding_bugs = 0
        for sp in nx.neighbors(G, p):
            surrounding_bugs += grid[sp]
        
        if bug == 1:
            new_grid[p] = 1 if surrounding_bugs == 1 else 0
        elif bug == 0 and (surrounding_bugs == 1 or surrounding_bugs == 2):
            new_grid[p] = 1
        else:
            new_grid[p] = bug

    return new_grid

grid = get_grid()
minutes = 200

G = get_layers(-100, 100)
print(sum(grid.values()))

for i in range(minutes):
    grid = update_grid(grid, G)

print(sum(grid.values()))
