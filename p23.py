from intcode_vm import VM, YieldCode
from collections import deque

def get_program():
    return list(map(int, open("data23.txt").read().strip().split(',')))

class NetworkPC():

    def __init__(self, address):
        self.address = address

        self.vm = VM(get_program())
        self.p = self.vm.run_intcode()
        x = next(self.p)
        y = self.p.send(self.address)
        # self.log(x)
        # self.log(y)

    def log(self, *args):
        print(self.address, *args)

    def run(self, arg_lst):
        for a in arg_lst:
            x = self.p.send(a)
            # self.log(x)

        output = []
        while x != YieldCode.Input:
            output.append(x)
            x = next(self.p)
            # self.log(x)

        return output

def main():

    pcs = [NetworkPC(i) for i in range(50)]
    queues = [deque() for _ in range(50)]

    nat = None
    last_sent_nat_y = None

    while True:
        for i in range(50):
            if queues[i]:
                to_send = queues[i].popleft()
            else:
                to_send = [-1]
            
            output = pcs[i].run(to_send)
            for address, x, y in zip(*[iter(output)]*3):
                if address == 255:
                    if nat is None:
                        print(y)
                    nat = (x, y)
                else:
                    queues[address].append((x, y))

        if not any(queues):
            if nat[1] == last_sent_nat_y:
                print(last_sent_nat_y)
                return
            queues[0].append(nat)
            last_sent_nat_y = nat[1]

main()
