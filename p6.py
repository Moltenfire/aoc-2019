

def get_input():
    with open("p6.txt", 'r') as f:
        return f.readlines()

def get_count(direct_orbits, s, e="COM"):
    i = 0
    c = s
    l = [c]

    while c != e:
        c = direct_orbits[c]
        i += 1
        l.append(c)

    return i, list(reversed(l))

def get_num_common(l0, l1):
    for i in range(min(len(l0), len(l1))):
        if l0[i] != l1[i]:
            return i - 1

direct_orbits = {}
all_objects = set()
for l in get_input():
    a, b = l.strip().split(")")
    direct_orbits[b] = a
    all_objects.add(a)
    all_objects.add(b)

# i = 0
# for ob in all_objects:
#     c, _ = get_count(direct_orbits, ob)
#     i += c
# print(i)

_, l0 = get_count(direct_orbits, "YOU")
_, l1 = get_count(direct_orbits, "SAN")

common = get_num_common(l0, l1)

s = len(l0) + len(l1) - 2 *common - 4

print(s)
