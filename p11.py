import cmath
from intcode_vm import VM, YieldCode
from collections import defaultdict

program = list(map(int, open("data11.txt").read().strip().split(',')))

directions = [
    complex(-1, 0), # L
    complex(0, 1), # U
    complex(1, 0), # R
    complex(0, -1) # D
]

def run_robot(initial):
    direction = 1
    positon = complex(0, 0)
    panel = defaultdict(int)

    panel[positon] = initial

    vm = VM(program)
    p = vm.run_intcode()

    while next(p) == YieldCode.Input:
        colour = p.send(panel[positon])
        turn = next(p)

        panel[positon] = colour
        turn_delta = -1 if turn == 0 else 1
        direction = (direction + turn_delta) % 4
        positon += directions[direction]

    return panel


print(len(run_robot(0)))

panel = run_robot(1)

x_min = int(min(panel.keys(), key=lambda x: x.real).real)
x_max = int(max(panel.keys(), key=lambda x: x.real).real)
y_min = int(min(panel.keys(), key=lambda x: x.imag).imag)
y_max = int(max(panel.keys(), key=lambda x: x.imag).imag)

for y in range(y_max, y_min-1, -1):
    l = []
    for x in range(x_min, x_max+1):
        pixel = panel[complex(x,y)]
        l.append(pixel)

    print(''.join(map(str, l)).replace('0', '  ').replace('1', '##'))
