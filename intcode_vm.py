from enum import Enum
from collections import defaultdict

class YieldCode(Enum):
    Output = 0
    Input = 1
    Halting = 2

def decode(i):
    inst = i % 100
    a = (i // 100) % 10
    b = (i // 1000) % 10
    c = (i // 10000) % 10

    return inst, [a,b,c]

class VM():
    def __init__(self, program):
        self.program = defaultdict(int, enumerate(program))
        self.relative_base = 0

    def get_value(self, index, mode):
        if mode == 0:
            return self.program[self.program[index]]
        elif mode == 1:
            return self.program[index]
        elif mode == 2:
            return self.program[self.program[index] + self.relative_base]

    def write_value(self, index, mode, value):
        if mode == 0:
            self.program[self.program[index]] = value
        elif mode == 2:
            self.program[self.program[index] + self.relative_base] = value
        
    def run_intcode(self):
        i = 0
        output_value = 0

        running = True

        instructions = 0

        while running:
            instruction, modes = decode(self.program[i])
            instructions += 1

            if instruction == 99:
                running = False
                yield YieldCode.Halting
            elif instruction == 1:
                p = self.get_value(i+1, modes[0])
                q = self.get_value(i+2, modes[1])
                r = p + q
                self.write_value(i+3, modes[2], r)
                i += 4
            elif instruction == 2:
                p = self.get_value(i+1, modes[0])
                q = self.get_value(i+2, modes[1])
                r = p * q
                self.write_value(i+3, modes[2], r)
                i += 4
            elif instruction == 3:
                in_value = yield YieldCode.Input
                self.write_value(i+1, modes[0], in_value)
                i += 2
            elif instruction == 4:
                out_value = self.get_value(i+1, modes[0])
                yield out_value
                i += 2
            elif instruction == 5:
                p = self.get_value(i+1, modes[0])
                q = self.get_value(i+2, modes[1])
                i = q if p != 0 else i + 3
            elif instruction == 6:
                p = self.get_value(i+1, modes[0])
                q = self.get_value(i+2, modes[1])
                i = q if p == 0 else i + 3
            elif instruction == 7:
                p = self.get_value(i+1, modes[0])
                q = self.get_value(i+2, modes[1])
                r = 1 if p < q else 0
                self.write_value(i+3, modes[2], r)
                i += 4
            elif instruction == 8:
                p = self.get_value(i+1, modes[0])
                q = self.get_value(i+2, modes[1])
                r = 1 if p == q else 0
                self.write_value(i+3, modes[2], r)
                i += 4
            elif instruction == 9:
                self.relative_base += self.get_value(i+1, modes[0])
                i += 2
