import networkx as nx

def get_edges():
    with open("p6.txt", 'r') as f:
        return (tuple(line.strip().split(")")) for line in f.readlines())

G = nx.Graph(get_edges())

shortest_paths = nx.shortest_path_length(G, "COM")
print(sum(shortest_paths.values()))
print(nx.shortest_path_length(G, "YOU", "SAN") - 2)
