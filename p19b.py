from intcode_vm import VM, YieldCode
from collections import namedtuple
from itertools import count

Point = namedtuple('Point', 'x y')

def get_program():
    return list(map(int, open("data19.txt").read().strip().split(',')))

program = get_program()

def check_pos(point):
    vm = VM(program[:])
    p = vm.run_intcode()
    next(p)
    p.send(point.x)
    res = p.send(point.y)

    # print(point, res)
    return res

target = 100
y = 10

while True:
    # print(y)
    # find first beam
    x_left = 0
    while not check_pos(Point(x_left, y)):
        x_left += 1

    x_right = x_left
    while check_pos(Point(x_right, y)):
        x_right += 1
    width = x_right - x_left

    if width >= target:
        bottom_left = Point(x_right - target, y + target - 1)
        if check_pos(bottom_left):
            while True:
                test_bottom_left = Point(bottom_left.x - 1, bottom_left.y)
                if check_pos(test_bottom_left):
                    bottom_left = test_bottom_left
                else:
                    break
            print(Point(bottom_left.x, y))
            break

    print(y, width)

    y += 1
