from collections import Counter

def valid_num1(n):
    n = str(n)
    dub = False
    for i in range(len(n)-1):
        x = n[i]
        y = n[i+1]

        if x > y:
            return False
        if x == y:
            dub = True

    return dub

def valid_num2(n):
    n = str(n)
    i = 0
    dubs = 0

    for i in range(len(n)-1):
        x = n[i]
        y = n[i+1]
        if x > y:
            return False

    counts = Counter(n)

    for x, y in counts.most_common():
        if y == 2:
            return True

    return False




count = 0
for i in range(156218, 652528):
    if valid_num2(i):
        count += 1

print(count)

# print(valid_num2(111122))
# print(valid_num2(123444))
