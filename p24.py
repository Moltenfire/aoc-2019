from collections import defaultdict, namedtuple
from pprint import pprint
from itertools import count

Point = namedtuple('P', 'x y')

def get_grid():
    with open("data24.txt") as f:
        grid = defaultdict(int)
        for y, line in enumerate(f.readlines()):
            line = line.strip()
            for x, c in enumerate(line):
                grid[Point(x,y)] = 1 if c == "#" else 0
    return grid

def print_grid(grid):
    for y in range(5):
        for x in range(5):
            c = '#' if grid[Point(x,y)] else '.'
            print(c, end='')
        print()

def get_surrounding_points(p):
    return set([
        Point(p.x, p.y-1),
        Point(p.x, p.y+1),
        Point(p.x-1, p.y),
        Point(p.x+1, p.y),
    ])

def update_grid(grid):
    new_grid = defaultdict(int)

    for p, bug in list(grid.items()):
        surrounding_bugs = 0
        for sp in get_surrounding_points(p):
            surrounding_bugs += grid[sp]
        
        if bug == 1:
            new_grid[p] = 1 if surrounding_bugs == 1 else 0
        elif bug == 0 and (surrounding_bugs == 1 or surrounding_bugs == 2):
            new_grid[p] = 1
        else:
            new_grid[p] = bug

    return new_grid

def biodiversity_rating(grid):
    rating = 0
    for y in range(5):
        for x in range(5):
            if grid[Point(x,y)]:
                rating += 1 << (5*y + x)
    return rating

grid = get_grid()

ratings = set([biodiversity_rating(grid)])

for i in count():
    grid = update_grid(grid)
    rating = biodiversity_rating(grid)
    if rating in ratings:
        print(i)
        print(rating)
        break
    ratings.add(rating)

# print_grid(grid)





