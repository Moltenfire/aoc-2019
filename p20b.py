from collections import defaultdict, namedtuple, deque
from pprint import pprint
import networkx as nx

Point = namedtuple('P', 'x y')
LPoint = namedtuple('LP', 'x y l')

with open("data20.txt") as f:
    lines = list(map(lambda x : list(x), f.readlines()))

    grid = {}
    path = defaultdict(int)
    for y, line in enumerate(lines):
        if line[-1] == '\n':
            line = line[:-1]
        for x, c in enumerate(line):
            grid[Point(x,y)] = c
            if c == '.':
                path[Point(x,y)] = 1

    width = len(lines[-1])
    height = len(lines)
    mid_x = (width - 1) // 2
    mid_y = (height - 1) // 2

    # print(width, height)
    # print(mid_x, mid_y)

    top_height = 0
    bottom_height = 0
    for y in range(mid_y):
        c = grid[Point(mid_x, y)]
        if c == '#' or c == '.':
            top_height += 1

        c = grid[Point(mid_x, height - y - 1)]
        if c == '#' or c == '.':
            bottom_height += 1

    left_width = 0
    right_width = 0
    for x in range(mid_x):
        c = grid[Point(x, mid_y)]
        if c == '#' or c == '.':
            left_width += 1

        c = grid[Point(width - x - 1, mid_y)]
        if c == '#' or c == '.':
            right_width += 1

    teleports = defaultdict(list)

    for x in range(2, width - 2):
        p = Point(x, 2)
        if path[p]:
            letters = ''.join(sorted([grid[Point(x, 0)], grid[Point(x, 1)]]))
            teleports[letters].append(p)
            # print(letters)

    for x in range(2, width - 2):
        p = Point(x, height - 3)
        # print(grid[p])
        if path[p]:
            letters = ''.join(sorted([grid[Point(x, height - 2)], grid[Point(x, height - 1)]]))
            teleports[letters].append(p)

    for y in range(2, height - 2):
        p = Point(2, y)
        if path[p]:
            letters = ''.join(sorted([grid[Point(0, y)], grid[Point(1, y)]]))
            teleports[letters].append(p)
            # print(letters)

    for y in range(2, height - 2):
        p = Point(width- 3, y)
        if path[p]:
            letters = ''.join(sorted([grid[Point(width - 2, y)], grid[Point(width - 1, y)]]))
            teleports[letters].append(p)
            # print(letters)

    for x in range(2 + left_width, width - right_width - 2):
        y = top_height + 1
        p = Point(x, y)
        if path[p]:
            letters = ''.join(sorted([grid[Point(x, y + 1)], grid[Point(x, y + 2)]]))
            teleports[letters].append(p)
            # print(letters)

    for x in range(2 + left_width, width - right_width - 2):
        y = height - top_height - 2
        p = Point(x, y)
        if path[p]:
            letters = ''.join(sorted([grid[Point(x, y - 1)], grid[Point(x, y - 2)]]))
            teleports[letters].append(p)
            # print(letters)

    for y in range(2 + top_height, height - top_height - 2):
        x = left_width + 1
        p = Point(x, y)
        if path[p]:
            letters = ''.join(sorted([grid[Point(x + 1, y)], grid[Point(x + 2, y)]]))
            teleports[letters].append(p)
            # print(letters)

    for y in range(2 + top_height, height - top_height - 2):
        x = width - right_width - 2
        p = Point(x, y)
        # print(grid[p], end='')
        if path[p]:
            letters = ''.join(sorted([grid[Point(x - 1, y)], grid[Point(x - 2, y)]]))
            teleports[letters].append(p)
            # print(letters)

def get_surrounding_points(p):
    return set([
        Point(p.x, p.y-1),
        Point(p.x, p.y+1),
        Point(p.x-1, p.y),
        Point(p.x+1, p.y),
    ])

def p_to_lp(p, layer):
    return LPoint(p.x, p.y, layer)

# pprint(teleports)

start_point = p_to_lp(teleports['AA'][0], 0)
end_point = p_to_lp(teleports['ZZ'][0], 0)

del teleports['AA']
del teleports['ZZ']

num_layers = len(teleports)

edges = set()

path_points = [p for p, v in path.items() if v]
for p in path_points:
    for sp in get_surrounding_points(p):
        if path[sp]:
            for layer in range(num_layers):
                edge = tuple(sorted([p_to_lp(p, layer), p_to_lp(sp, layer)]))
                edges.add(edge)

# Add teleports
for layer in range(num_layers):
    for points in teleports.values():
        p_outer, p_inner = points
        edge = (p_to_lp(p_outer, layer+1), p_to_lp(p_inner, layer))
        edges.add(edge)

G = nx.Graph(list(edges))

length = nx.shortest_path_length(G, start_point, end_point)
print(length)
