

def get_numbers():
    with open("data1.txt", 'r') as f:
        return list(map(int, f.readlines()))

def fuel_calc(m):
    return max(int(m / 3) - 2, 0)

def fuel_calc2(m):
    fuel_last = fuel_calc(m)
    fuel_total = fuel_last

    while fuel_last > 0:
        fuel_last = fuel_calc(fuel_last)
        fuel_total += fuel_last

    return fuel_total

res1 = sum(map(fuel_calc, get_numbers()))
res2 = sum(map(fuel_calc2, get_numbers()))

print(res1)
print(res2)
