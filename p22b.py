from collections import namedtuple
from itertools import count
from pprint import pprint

Instruction = namedtuple("Instruction", 'id value')

def get_instructions():
    instructions = []

    with open("data22.txt") as f:
        for line in f.readlines():
            line = line.strip()
            
            if line == "deal into new stack":
                instructions.append(Instruction(0, 0))
            else:
                s = line.split()
                first = s[0]
                if first == "Result:":
                    # print("Ex:", list(map(int, s[1:])))
                    break

                value = int(s[-1])
                if first == "cut":
                    instructions.append(Instruction(1, value))
                if first == "deal":
                    instructions.append(Instruction(2, value))

    return instructions

def find_func_values(instructions, size):
    addi = 0
    multi = 1

    for instruction in instructions:
        print(instruction)
        if instruction.id == 0: # Reverse
            multi *= -1
            addi += multi
        elif instruction.id == 1: # Cut
           addi += instruction.value * multi
        elif instruction.id == 2: # Deal Modulo
            multi *= pow(instruction.value, -1, size)

    return addi, multi

position = 2020
n_cards = 119315717514047
n_shuffles = 101741582076661
addi, multi = find_func_values(get_instructions(), n_cards)

combined_multi = pow(multi, n_shuffles, n_cards)
combined_addi = addi * (1 - pow(multi, n_shuffles, n_cards)) * pow(1 - multi, -1, n_cards)

print(multi)
print(addi)
print()
print(combined_multi)
print(combined_addi)

print((position * combined_multi + combined_addi) % n_cards)
