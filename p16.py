from itertools import cycle, islice
import numpy as np

def get_list():
    x = "59713137269801099632654181286233935219811755500455380934770765569131734596763695509279561685788856471420060118738307712184666979727705799202164390635688439701763288535574113283975613430058332890215685102656193056939765590473237031584326028162831872694742473094498692690926378560215065112055277042957192884484736885085776095601258138827407479864966595805684283736114104361200511149403415264005242802552220930514486188661282691447267079869746222193563352374541269431531666903127492467446100184447658357579189070698707540721959527692466414290626633017164810627099243281653139996025661993610763947987942741831185002756364249992028050315704531567916821944"
    return list(map(int, x)), int(x[:7])

PATTERN = [0, 1, 0, -1]
def pattern_repater(i):
    for j in cycle(PATTERN):
        for _ in range(i):
            yield j

def get_pattern(i, length):
    return np.array(list(islice(pattern_repater(i), 1, length + 1)))

def FFT(input_values):
    length = len(input_values)
    patterns = np.array([get_pattern(i+1, length) for i in range(length)])
    for _ in range(100):
        input_values =  np.remainder(np.absolute(np.dot(patterns, input_values)), 10)

    return input_values

def FFT2(input_values):
    for _ in range(100):
        input_values = np.flipud(np.flipud(input_values).cumsum())
        input_values = np.remainder(input_values, 10)

    return input_values

def part1():
    x, _ = get_list()
    res = FFT(np.array(x))
    print(''.join(map(str, res[:8])))

def part2():
    x, offset = get_list()
    x = x * 10000
    x = np.array(x[offset:])
    res = FFT2(x)
    res = int(''.join(map(str, res[:8])))
    print(res)

part1()
part2()
