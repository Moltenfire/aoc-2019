from intcode_vm import VM, YieldCode

class AsciiVM():

    def __init__(self, program, verbose=False):
        self.vm = VM(program)
        self.verbose = verbose
        self.p = self.vm.run_intcode()
        self.everything = []
        self.output = []
        self.running = True

        x = next(self.p)
        while True:
            if x == YieldCode.Input:
                break
            if x == YieldCode.Halting:
                self.running = False
                break
            c = chr(x)
            if self.verbose:
                print(c, end='')
            self.output.append(c)
            x = next(self.p)

    def run(self, command):
        command = command + "\n"
        for c in command:
            if self.verbose:
                print(c, end='')

            i = ord(c)
            self.everything.append(i)
            x = self.p.send(i)

        while True:
            if x == YieldCode.Input:
                break
            if x == YieldCode.Halting:
                if self.verbose:
                    print()
                self.running = False
                break
            try:
                c = chr(x)
            except:
                c = x
            if self.verbose:
                print(c, end='')
            self.output.append(c)
            x = next(self.p)
