from collections import namedtuple
from itertools import count
from tqdm import tqdm
from pprint import pprint

Instruction = namedtuple("Instruction", 'id value')

def get_instructions():
    instructions = []

    with open("data22.txt") as f:
        for line in f.readlines():
            line = line.strip()
            
            if line == "deal into new stack":
                instructions.append(Instruction(0, 0))
            else:
                s = line.split()
                first = s[0]
                if first == "Result:":
                    # print("Ex:", list(map(int, s[1:])))
                    break

                value = int(s[-1])
                if first == "cut":
                    instructions.append(Instruction(1, value))
                if first == "deal":
                    instructions.append(Instruction(2, value))

    return instructions


def shuffle(n_cards, instructions, n_times=1):
    cards = list(range(n_cards))

    # for _ in tqdm(range(n_times)):
    for _ in range(n_times):
        for instruction in instructions:
            # print(cards)
            if instruction.id == 0:
                cards = list(reversed(cards))
            elif instruction.id == 1:
                cut_value = instruction.value
                cards = cards[cut_value:] + cards[:cut_value]
            elif instruction.id == 2:
                new_cards = cards[:]
                for i, c in enumerate(cards):
                    new_cards[(i * instruction.value) % n_cards] = c
                    cards = new_cards

    return cards

def create_deal_map(n_cards, value):
    card_map = {}
    for i in range(n_cards):
        card_map[(i * value) % n_cards] = i
    return card_map

def egcd(a, b):
    if a == 0:
        return (b, 0, 1)
    else:
        g, y, x = egcd(b % a, a)
        return (g, x - (b // a) * y, y)

def modinv(a, m):
    g, x, y = egcd(a, m)
    if g != 1:
        raise Exception('modular inverse does not exist')
    else:
        return x % m

def shuffle_position(n_cards, n_shuffles, instructions, position):
    rev_instructions = list(reversed(instructions))
    start_position = position

    # card_maps = {}
    # for instruction in instructions:
    #     if instruction.id == 2:
    #         card_maps[instruction.value] = create_deal_map(n_cards, instruction.value)

    # for n in range(n_shuffles):
    positions = [position]
    # print(0, position)
    for n in count(1):
        for instruction in rev_instructions:
            if instruction.id == 0:
                position = n_cards - position - 1
            elif instruction.id == 1:
                position = (instruction.value + position) % n_cards
            elif instruction.id == 2:
                position = modinv(position, n_cards)

        # print(n, position)
        if start_position == position:
            break
        else:
            positions.append(position)

    total = len(positions)
    print(total)

    x = n_shuffles % total
    # print(total - x)

    position = positions[total - x]

    # print(set(range(n_cards)).difference(set(positions)))
    
    return position


# res = shuffle(10007, get_instructions())
# print(res.index(2019))


print(shuffle(10, get_instructions(), 1))
print(shuffle(10, get_instructions(), 2))
print(shuffle(10, get_instructions(), 3))
print(shuffle(10, get_instructions(), 4))

res = shuffle_position(10, 104, get_instructions(), 0)
print(res)

# res = shuffle_position(10007, 101741582076661, get_instructions(), 2020)
# res = shuffle_position(10007, 101741582076661, get_instructions(), 20)
# print(res)

# res = shuffle(10007, get_instructions(), 2423)

# print(res)
# print(res[2020])