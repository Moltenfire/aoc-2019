from intcode_vm import VM, YieldCode
from collections import namedtuple, defaultdict
from itertools import count

Point = namedtuple('Point', 'x y')

def get_program():
    return list(map(int, open("data19.txt").read().strip().split(',')))

class DroneSender():
    def __init__(self, program=get_program()):
        self.program = program
        self.drones_sent = 0

    def check_pos(self, point):
        self.drones_sent += 1
        vm = VM(self.program[:])
        p = vm.run_intcode()
        next(p)
        p.send(point.x)
        return p.send(point.y)

def count_tractor_beam_in_square(size):
    drone_sender = DroneSender()
    counter = 0
    last_y_with_no_beam = None
    for y in range(size):
        sub_counter = sum((drone_sender.check_pos(Point(x,y)) for x in range(size)))
        counter += sub_counter
        if not sub_counter:
            last_y_with_no_beam = y

    return counter, last_y_with_no_beam

def find_point_with_square(start_y, size):
    drone_sender = DroneSender()
    last_x_left = 0
    last_x_right = 0

    for y in count(start_y):
        x_left = last_x_left
        while not drone_sender.check_pos(Point(x_left, y)):
            x_left += 1

        x_right = max(x_left, last_x_right)
        while drone_sender.check_pos(Point(x_right, y)):
            x_right += 1

        last_x_left = x_left
        last_x_right = x_right
        width = x_right - x_left

        if width >= size:
            bottom_left = Point(x_right - size, y + size - 1)
            if drone_sender.check_pos(bottom_left):
                while True:
                    test_bottom_left = Point(bottom_left.x - 1, bottom_left.y)
                    if drone_sender.check_pos(test_bottom_left):
                        bottom_left = test_bottom_left
                    else:
                        break
                
                top_left_point = Point(bottom_left.x, y)
                return top_left_point, drone_sender.drones_sent

counter, last_y_with_no_beam = count_tractor_beam_in_square(size=50)
print(counter)

start_y = last_y_with_no_beam+1
point, drones_sent = find_point_with_square(start_y, size=100)

print(point, 10000 * point.x + point.y)
print("Drones used:", drones_sent)
