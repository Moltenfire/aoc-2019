from itertools import count, combinations
from math import gcd
import numpy as np

def get_data():
    px = []
    py = []
    pz = []
    vx = []
    vy = []
    vz = []

    with open("data12.txt") as f:
        for line in f.readlines():
            line = list(map(lambda x : int(x.strip()[2:]), line.strip()[1:-1].split(',')))
            px.append(line[0])
            py.append(line[1])
            pz.append(line[2])
            vx.append(0)
            vy.append(0)
            vz.append(0)

    return px, py, pz, vx, vy, vz

def velocity_change(a, b):
    return 0 if a == b else 1 if b > a else -1

def get_state_after_steps(p, v, i):
    total = len(p)

    ij = list(combinations(range(total), 2))

    for step in range(i):
        for i, j in ij:
            v[i] += velocity_change(p[i], p[j])
            v[j] += velocity_change(p[j], p[i])

        for i in range(total):
            p[i] += v[i]

def get_energy():

    px, py, pz, vx, vy, vz = get_data()

    get_state_after_steps(px, vx, 1000)
    get_state_after_steps(py, vy, 1000)
    get_state_after_steps(pz, vz, 1000)

    energy = 0
    for i in range(4):
        pot = abs(px[i]) + abs(py[i]) + abs(pz[i])
        kin = abs(vx[i]) + abs(vy[i]) + abs(vz[i])

        energy += pot * kin

    return energy

def get_first_repeat_step_coord(p, v):
    total = len(p)

    ij = list(combinations(range(total), 2))

    for step in count(1):
        for i, j in ij:
            v[i] += velocity_change(p[i], p[j])
            v[j] += velocity_change(p[j], p[i])

        for i in range(total):
            p[i] += v[i]

        if not any(v):
            return step * 2

def lcm(a, b):
    return a * b // gcd(a, b)

def get_first_repeat_step():

    px, py, pz, vx, vy, vz = get_data()

    x = get_first_repeat_step_coord(px, vx)
    y = get_first_repeat_step_coord(py, vy)
    z = get_first_repeat_step_coord(pz, vz)

    xy = lcm(x, y)
    return lcm(xy, z)

print(get_energy())
print(get_first_repeat_step())
