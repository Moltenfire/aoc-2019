
with open("data8.txt") as f:
    image = f.read().strip()

width = 25
height = 6
area = width * height

layers = []
check_sums = []
for i in range(0, len(image), area):
    layer = image[i:i+area]
    layers.append(layer)

    check_sums.append((layer.count("0"), layer.count("1") * layer.count("2")))

print(min(check_sums)[1])

output = []
for i in range(area):
    for layer in layers:
        pixel = layer[i]
        if pixel != '2':
            output.append(pixel)
            break

for i in range(0, area, width):
    print(''.join(output[i:i+width]).replace('0', '  ').replace('1', '##'))
