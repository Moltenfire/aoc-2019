from asciicode_vm import AsciiVM
from itertools import combinations

def get_program():
    return list(map(int, open("data25.txt").read().strip().split(',')))

items = [
"weather machine",
"shell",
"candy cane",
"whirled peas",
"hypercube",
"space law space brochure",
"space heater",
"spool of cat6"
]

def collect_items(indexes):
    # vm = AsciiVM(get_program())
    vm = AsciiVM(get_program(), True)

    commands = [
        "west",
        "east",
        "north",
        "south",
        "south",
        "take spool of cat6",
        "west",
        "take space heater",
        "north",
        "take weather machine",
        "north",
        "west",
        # # "take escape pod",
        "west",
        "take whirled peas",
        "east",
        "east",
        "south",
        "west",
        # "take giant electromagnet",
        "south",
        # "take photons",
        "south",
        "take space law space brochure",
        "west",
        # "take infinite loop",
        "east",
        "north",
        "east",
        "take candy cane",
        "west",
        "north",
        "east",
        "south",

        "south",
        "take shell",
        "north",
        "east",
        "east",
        # # "take molten lava",
        "south",
        "take hypercube",
        "south",
        "south",
        "inv"
    ]

    for cmd in commands:
        vm.run(cmd)

    for i in indexes:
        vm.run("drop " + items[i])

    vm.run("inv")
    vm.run("east")

    # print(indexes, end='')
    # lines = list(filter(lambda x : "Droids" in x, (''.join(vm.output).split('\n'))))
    # print(lines[0])



# for x in range(len(items)):
#     for i in combinations(range(len(items)), x):
#         collect_items(i)

# collect_items([])

collect_items([3, 5, 6, 7])