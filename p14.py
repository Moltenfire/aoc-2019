import networkx as nx
from collections import defaultdict, namedtuple
from pprint import pprint
from math import ceil, floor

Transform = namedtuple('Transform', 'c n')


def get_data():
    with open("data14.txt") as f:

        transforms = defaultdict(list)
        transforms_from = defaultdict(list)
        transform_required = {}
        for line in f.readlines():

            f, t = line.strip().split('=>')

            t_n, t_c = t.strip().split()
            transform_required[t_c] = int(t_n)

            for n, c in map(lambda x : x.strip().split(), f.split(',')):
                p = Transform(c, int(n))
                transforms[c].append(t_c)
                transforms_from[t_c].append(p)

        return transforms, transforms_from, transform_required

def get_max_distance_from_ore(transforms):
    max_distance_from_ore = defaultdict(int)
    remaining = set(['ORE'])

    while len(remaining):
        x = remaining.pop()
        v = max_distance_from_ore[x] + 1

        for y in transforms[x]:
            if v > max_distance_from_ore[y]:
                max_distance_from_ore[y] = v
            remaining.add(y)

    del max_distance_from_ore['ORE']
    return dict(max_distance_from_ore)

def get_fuel_in_ore(exact=False):

    transforms, transforms_from, transform_required = get_data()

    max_distance_from_ore = get_max_distance_from_ore(transforms)

    current_items = defaultdict(int, {'FUEL' : 1})

    for x in sorted(max_distance_from_ore, key=lambda x : max_distance_from_ore[x], reverse=True):

        times = current_items[x] / transform_required[x]
        if not exact:
            times = ceil(times)

        for y in transforms_from[x]:
            current_items[y.c] += (y.n * times)

    print(current_items)
    return current_items['ORE']

ore = get_fuel_in_ore()
print(ore)

exact_ore = get_fuel_in_ore(exact=True)
print(exact_ore)
print(floor(1000000000000 / exact_ore))
