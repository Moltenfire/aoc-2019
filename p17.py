from asciicode_vm import AsciiVM
from collections import defaultdict, namedtuple
from itertools import count

Point = namedtuple('Point', 'x y')
Move = namedtuple('Move', 'turn distance')

def get_program():
    return list(map(int, open("data17.txt").read().strip().split(',')))

def get_grid(program):
    vm = AsciiVM(program)
    characters = vm.output

    grid = defaultdict(int)
    x = 0
    y = 0
    start_pos = None
    for c in characters:
        if c == '\n':
            x = 0
            y += 1
        elif c == '#':
            grid[Point(x,y)] = 1
            x += 1
        elif c != '.':
            start_pos = Point(x,y)
            grid[start_pos] = c
            x += 1
        else:
            x += 1

    return grid, start_pos

def get_surrounding_points(p):
    return set([
        Point(p.x, p.y-1),
        Point(p.x, p.y+1),
        Point(p.x-1, p.y),
        Point(p.x+1, p.y),
    ])

def get_alignment(grid):
    total = 0
    for p in grid:
        sp = get_surrounding_points(p)
        if all(s in grid for s in sp):
            total += (p.x * p.y)

    return total

def add_point(p0, p1):
    return Point(p0.x + p1.x, p0.y + p1.y)

def get_directions(grid, start_point):
    directions = [
        Point(0, -1),
        Point(1, 0),
        Point(0, 1),
        Point(-1, 0)
    ]

    direction = 1
    distance = 0
    turns = ['R']
    distances = []
    current_point = start_point
    while True:
        next_point = add_point(current_point, directions[direction])
        
        if grid[next_point]:
            distance += 1
            current_point = next_point
        else:
            distances.append(distance)
            distance = 0

            d_left = (direction - 1) % 4
            d_right = (direction + 1) % 4

            left_point = add_point(current_point, directions[d_left])
            right_point = add_point(current_point, directions[d_right])

            if grid[left_point]:
                turns.append('L')
                direction = d_left
            elif grid[right_point]:
                turns.append('R')
                direction = d_right
            else:
                return [Move(t, d) for t, d in zip(turns, distances)]

def count_pattern(lst, pattern):
    pattern_length = len(pattern)
    count = 0
    i = 0
    while i < len(lst):
        if lst[i:i+pattern_length] == pattern:
            count += 1
            i += pattern_length
        else:
            i += 1

    return count
    # return sum(1 for i in range(len(lst) - pattern_length + 1) )

def largest_pattern(remaining_moves):
        for i in range(4):
            pattern = remaining_moves[:i+1]
            # print(pattern, count_pattern(remaining_moves, pattern))
            if count_pattern(remaining_moves, pattern) == 1:
                return remaining_moves[:i]
        return remaining_moves[:4]

def find_subroutines(direction_moves):

    subroutines = []
    remaining_moves = direction_moves[:]

    while remaining_moves:
        pattern = largest_pattern(remaining_moves)
        pattern_length = len(pattern)
        subroutines.append(pattern)
        # remaining_moves = remaining_moves[len(pattern):]

        indexs = [i for i in range(len(remaining_moves) - pattern_length + 1) if remaining_moves[i:i+pattern_length] == pattern]
        for i in reversed(indexs):
            remaining_moves = remaining_moves[:i] + remaining_moves[i+pattern_length:]

    main = []
    i = 0
    while i < len(direction_moves):
        for j, s in enumerate(subroutines):
            x = direction_moves[i:i+len(s)]
            if s == x:
                main.append(j)
                i += len(s)
                break

    return main, subroutines

program = get_program()
grid, start_point = get_grid(program)
print(get_alignment(grid))

direction_moves = get_directions(grid, start_point)

main, subroutines = find_subroutines(direction_moves)

input_strs = []

input_strs.append(','.join((chr(i + 65) for i in main)))
subroutine_strs = []

for s in subroutines:
    s = ','.join("{},{}".format(m.turn, m.distance) for m in s)
    input_strs.append(s)

input_strs.append("n")

program = get_program()
program[0] = 2
vm = AsciiVM(program)

for i in input_strs:
    vm.run(i)

print(ord(vm.output[-1]))
